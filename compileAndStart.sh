#!/usr/bin/env bash
mvn clean install
STATUS=$?
if [ $STATUS -eq 0 ]; then
echo "Deployment Successful\n"
cd engine
mvn spring-boot:run
else
echo "Deployment Failed"
fi