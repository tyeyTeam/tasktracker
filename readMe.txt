++++ Requirements
+ Oracle Java 8 Runtime (tested on latest version)
+ Maven 3
+ Port 8080 available on the system

++++ Features
+ H2 console available through web at /console
+ Rest access to spring data repository features through
+ initial data for testing automatically loaded on startup


++++ REST Api for required use cases:
1. CRUD for Task and Employee:
    + Create resource:
        url: http://localhost:8080/tasks or employees (remember that employee contain only 'name' in JSON!)
        Method: POST
        Content-Type: application/json
        Body: {
                "name": "Test Task",
                "status": "ACTIVE"
              }
    + Read resource:
        url: http://localhost:8080/tasks/{id} or ../employees/{id}
        Method: GET
    + Update resource:
        url: http://localhost:8080/tasks/{id} or ../employees/{id}
        Method: PUT/PATCH
        Content-Type: application/json
        Body: {
                "name": "Test Task",
                "status": "COMPLETED"
              }
    + Delete resource:
        url: http://localhost:8080/tasks/{id} or ../employees/{id}
        Method: DELETE
2. Find all tasks:
        url: http://localhost:8080/tasks
        Method: GET
3. Find all active tasks for employee:
        url: http://localhost:8080/tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive?id={id}
        Method: GET
4. Find task for employee:
        url: http://localhost:8080/tasks/search/findFirstByEmployeeTasks_Employee_id?id={id}
        Method: GET
5. Find all tasks for employee where he spent more than four hours:
        url: http://localhost:8080/tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour?id={id}
        Method: GET
6. Find average time for all employee tasks:
        url: http://localhost:8080/employees/search/findAverageTimeSpentOnTasksByEmployee?id={id}
        Method: GET
7. Find all employees for a task
        url: http://localhost:8080/employees/search/findByEmployeeTasks_Task_id?id={id}
        Method: GET

++++ Quick Start
For quick start run 'compileAndStart.sh' in home directory or build executable jar with maven and run it with java (make sure that port 8080 is available).

