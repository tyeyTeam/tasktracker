package com.kantwert.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tyey on 26.03.16.
 */
@SpringBootApplication
@EnableAutoConfiguration
public class TaskTracker {

    public static void main(String[] args) {
        SpringApplication.run(TaskTracker.class, args);
    }
}
