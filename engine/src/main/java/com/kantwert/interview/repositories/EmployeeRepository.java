package com.kantwert.interview.repositories;

import com.kantwert.interview.model.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Task Repository. Below standard operations provided by Spring Data:
 * - CRUD for task
 * <p>
 * Created by tyey on 29.03.16.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    /**
     * Find all employees for a task.
     *
     * @param taskId
     * @return All employees for a task.
     */
    Set<Employee> findByEmployeeTasks_Task_id(@Param("id") long taskId);


    /**
     * Find average time for all employee tasks.
     *
     * @param employeeid
     * @return Average time for all employee tasks.
     */
    @Query("select AVG(et.timeSpent) from Employee e join e.employeeTasks et where et.employee.id=:id")
    Double findAverageTimeSpentOnTasksByEmployee(@Param("id") long employeeid);
}
