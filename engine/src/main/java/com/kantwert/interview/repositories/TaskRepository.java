package com.kantwert.interview.repositories;

import com.kantwert.interview.model.Task;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Task Repository. Below standard operations provided by Spring Data:
 * - CRUD for task
 * - Find all tasks
 * <p>
 * <p>
 * Created by tyey on 29.03.16.
 */
public interface TaskRepository extends CrudRepository<Task, Long> {

    /**
     * Find task for employee.
     *
     * @param employeeId
     * @return First Task found for given employee.
     */
    Task findFirstByEmployeeTasks_Employee_id(@Param("id") long employeeId);

    /**
     * Find all active tasks for employee.
     *
     * @param employeeId
     * @return All active Tasks for given employee.
     */
    @Query("select t from Task t join t.employeeTasks et where et.employee.id=:id and t.status='ACTIVE'")
    Set<Task> findByEmployeeTasks_Employee_idAndStatusIsActive(@Param("id") long employeeId);

    /**
     * Find all tasks for employee where he spent more than four hours.
     *
     * @param employeeId
     * @return All tasks for employee where he spent more than four hours.
     */
    @Query("select t from Task t join t.employeeTasks et where et.employee.id=:id and et.timeSpent>4")
    Set<Task> findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour(@Param("id") long employeeId);

}
