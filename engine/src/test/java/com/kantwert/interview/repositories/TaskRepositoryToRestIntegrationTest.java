package com.kantwert.interview.repositories;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by tyey on 29.03.16.
 */
public class TaskRepositoryToRestIntegrationTest extends AbstractRepositoryToRestIntegrationTest {

    /**
     * testing /tasks/search/findFirstByEmployeeTasks_Employee_id{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findFirstByEmployeeTasks_Employee_id").param("id", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$.name", Matchers.equalTo("JAK JA NIE CIERPIE SMERFOW!")))
                .andExpect(jsonPath("$.status", Matchers.equalTo("ACTIVE")));
    }

    /**
     * testing /tasks/search/findFirstByEmployeeTasks_Employee_id{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnFirstResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findFirstByEmployeeTasks_Employee_id").param("id", "4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$.name", Matchers.equalTo("ALE GLUPI CI RZYMIANIE")))
                .andExpect(jsonPath("$.status", Matchers.equalTo("ACTIVE")));
    }

    /**
     * testing /tasks/search/findFirstByEmployeeTasks_Employee_id{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnTaskNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findFirstByEmployeeTasks_Employee_id").param("id", "1"))
                .andExpect(status().isNotFound());
    }


    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnOneActiveTask() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive").param("id", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(1)))
                .andExpect(jsonPath("$._embedded.tasks[0].name", Matchers.equalTo("JAK JA NIE CIERPIE SMERFOW!")))
                .andExpect(jsonPath("$._embedded.tasks[0].status", Matchers.equalTo("ACTIVE")));
    }

    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnTwoActiveTask() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive").param("id", "4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(2)))
                .andExpect(jsonPath("$._embedded.tasks[0].name", Matchers.equalTo("ALE GLUPI CI RZYMIANIE")))
                .andExpect(jsonPath("$._embedded.tasks[0].status", Matchers.equalTo("ACTIVE")))
                .andExpect(jsonPath("$._embedded.tasks[1].name", Matchers.equalTo("ZJESC DZIKA")))
                .andExpect(jsonPath("$._embedded.tasks[1].status", Matchers.equalTo("ACTIVE")));
    }


    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldNotReturnActiveTasks() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndStatusIsActive").param("id", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(0)));
    }


    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnOneTaskWithTimeSpentGreaterThan4() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour").param("id", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(1)))
                .andExpect(jsonPath("$._embedded.tasks[0].name", Matchers.equalTo("JAK JA NIE CIERPIE SMERFOW!")))
                .andExpect(jsonPath("$._embedded.tasks[0].status", Matchers.equalTo("ACTIVE")));
    }

    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldReturnTwoTasksWithTimeSpentGreaterThan4() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour").param("id", "4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(2)))
                .andExpect(jsonPath("$._embedded.tasks[0].name", Matchers.equalTo("ALE GLUPI CI RZYMIANIE")))
                .andExpect(jsonPath("$._embedded.tasks[0].status", Matchers.equalTo("ACTIVE")))
                .andExpect(jsonPath("$._embedded.tasks[1].name", Matchers.equalTo("ZJESC DZIKA")))
                .andExpect(jsonPath("$._embedded.tasks[1].status", Matchers.equalTo("ACTIVE")));
    }


    /**
     * testing /tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour{?id}
     *
     * @throws Exception
     */
    @Test
    public void shouldNotReturnTasksWithTimeSpentGreaterThan4() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks/search/findByEmployeeTasks_Employee_idAndEmployeeTasks_TimeSpentGreaterThanFour").param("id", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.tasks", Matchers.hasSize(0)));
    }


}
