package com.kantwert.interview.repositories;


import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by tyey on 29.03.16.
 */
public class EmployeeRepositoryToRestIntegrationTest extends AbstractRepositoryToRestIntegrationTest {

    /**
     * testing /employees/search/findByEmployeeTasks_Task_id
     *
     * @throws Exception
     */
    @Test
    public void shouldReturn2Results() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/search/findByEmployeeTasks_Task_id").param("id", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.employees", Matchers.hasSize(2)))
                .andExpect(jsonPath("$._embedded.employees[0].name", Matchers.equalTo("ASTERIX")))
                .andExpect(jsonPath("$._embedded.employees[1].name", Matchers.equalTo("OBELIX")));
    }

    /**
     * testing /employees/search/findByEmployeeTasks_Task_id
     *
     * @throws Exception
     */
    @Test
    public void shouldNotReturnResults() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/search/findByEmployeeTasks_Task_id").param("id", "100"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._embedded.employees", Matchers.hasSize(0)));
    }

    /**
     * testing /employees/search/findByEmployeeTasks_Task_id
     *
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void shouldThrowException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/search/findByEmployeeTasks_Task_id"));
    }

    /**
     * testing /employees/search/findAverageTimeSpentOnTasksByEmployee
     *
     * @throws Exception
     */
    @Test
    public void shouldCalculateAverage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/search/findAverageTimeSpentOnTasksByEmployee").param("id", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$", Matchers.equalTo(5.0)));
    }

    /**
     * testing /employees/search/findAverageTimeSpentOnTasksByEmployee
     *
     * @throws Exception
     */
    @Test
    public void shouldNotCalculateAverageAndReturnNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/search/findAverageTimeSpentOnTasksByEmployee").param("id", "100"))
                .andExpect(status().isNotFound());
    }


}
