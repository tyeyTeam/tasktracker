package com.kantwert.interview.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tyey on 26.03.16.
 */
@Entity
@Table(name = "employee_tasks")
public class EmployeeTask implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private Task task;

    @Id
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;

    /**
     * Time in hours
     */
    @Column(name = "time_spent")
    private int timeSpent;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployeeTask that = (EmployeeTask) o;

        if (task != null ? !task.equals(that.task) : that.task != null) return false;
        return employee != null ? employee.equals(that.employee) : that.employee == null;

    }

    @Override
    public int hashCode() {
        int result = task != null ? task.hashCode() : 0;
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EmployeeTask{" +
                "task=" + task +
                ", employee=" + employee +
                ", timeSpent=" + timeSpent +
                '}';
    }
}
